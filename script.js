document.querySelector('#btnListar').addEventListener('click', listardatos);
document.querySelector('#btnLimpiar').addEventListener('click', limpiar);

function listardatos() {
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'datos.json', true);
    xhttp.send();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let datos = JSON.parse(this.responseText);
            let cuerpo = document.querySelector('#resultado');
            cuerpo.innerHTML = '';
            for (let elementos of datos) {
                cuerpo.innerHTML += `
                <tr>
                <td>${elementos.id}</td>
                <td>${elementos.cedula}</td>
                <td>${elementos.nombre}</td>
                <td>${elementos.direccion}</td>
                <td>${elementos.telefono}</td>
                <td>${elementos.correo}</td>
                <td>${elementos.curso}</td>
                <td>${elementos.paralelo}</td>
                </tr>
                `

            }
        }
    }
}
function limpiar(){
    window.location.href = 'index.html'
}